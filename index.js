require('dotenv').config();

const Koa = require('koa');
const session = require('koa-session');
const mount = require('koa-mount');
const helmet = require('koa-helmet');
const Router = require('koa-router');
const views = require('koa-views');
const bodyParser = require('koa-bodyparser');
const request = require('request-promise-native');
const querystring = require('querystring');
const crypto = require('crypto');

const app = new Koa();
const router = new Router();

// Create KeyGrip instance
app.keys = ['grant'];

app.use(bodyParser());
app.use(helmet());
app.use(session(app));
app.use(views(__dirname + '/views'));
app.use(router.routes());
app.use(router.allowedMethods());

router.get('/', async (ctx, next) => {
  await ctx.render('home.html');
});

router.get('/login', async (ctx, next) => {
  const state = crypto.randomBytes(64).toString('hex');
  ctx.session.state = state;
  const qs = querystring.stringify({
    client_id: process.env.SPOTIFY_CLIENT,
      response_type: 'code',
      redirect_uri: process.env.URI + '/spotify/callback',
      state: state,
      scope: 'playlist-read-private',      
  });
  ctx.redirect('https://accounts.spotify.com/authorize/?' + qs);
});

router.get('/spotify/callback', (ctx, next) => {
  const code = ctx.query.code || null;
  const state = ctx.query.state || null;
  const storedState = ctx.session.state ? ctx.session.state : null;

  // If state isn't the same we aren't sure if it came from original request
  if (state === null || state !== storedState) {
    ctx.session = null;
    ctx.redirect('/');
  }
  console.log()
  const options = {
    method: 'POST',
    uri: 'https://accounts.spotify.com/api/token',
    form: {
      grant_type: 'authorization_code',
      code: ctx.query.code,
      redirect_uri: process.env.URI + '/spotify/callback',
      client_id: process.env.SPOTIFY_CLIENT,
      client_secret: process.env.SPOTIFY_SECRET,
    },
    json: true,
  };
  return request(options)
    .then((body) => {
      ctx.session.access_token = body.access_token;
      ctx.session.refresh_token = body.refresh_token;
      ctx.redirect('/dashboard');
    })
    .catch((err) => {
      ctx.redirect('/');
    });
});

router.get('/dashboard', (ctx, next) => {
  ctx.body = ctx.session;
});

app.listen(4500);

var options = {
  uri: 'https://api.github.com/user/repos',
  qs: {
      access_token: 'xxxxx xxxxx' // -> uri + '?access_token=xxxxx%20xxxxx'
  },
  headers: {
      'User-Agent': 'Request-Promise'
  },
  json: true // Automatically parses the JSON string in the response
};